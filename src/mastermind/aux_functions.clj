(ns mastermind.aux_functions)
(require '[clojure.string :as str])

(defn code-secret [n]
  (let [colormap {0 :rouge, 1 :bleu, 2 :vert, 3 :jaune, 4 :noir, 5 :blanc}]
    (loop [n n, colorseq []]
      (if (zero? n)
        colorseq
        (recur (- n 1) (conj colorseq (colormap (rand-int 6))))))))

(defn indications [s t]
  (loop [its s, itt t, res []]
    (if (seq its)
      (if (=  (first its) (first itt))
        (recur (rest its) (rest itt) (conj res :good))
        (if (some #(= (first itt) %) s)
          (recur (rest its) (rest itt) (conj res :color))
          (recur (rest its) (rest itt) (conj res :bad))))
      res)))

(defn addmap [m v]
  (if (contains? m v)
    (assoc m v (+ (m v) 1))
    (assoc m v 1)))

(defn submap [m v]
  (if (contains? m v)
    (assoc m v (- (m v) 1))
    m))

(defn frequences [s]
  (loop [s s, resmap {}]
    (if (seq s)
      (let [v (first s)]
        (recur (rest s) (addmap resmap v)))
      resmap)))

(defn freqs-dispo [c i]
  (loop [itc c iti i resmap {}]
    (if (seq itc)
      (if (= :good (first iti))
        (recur (rest itc) (rest iti) (assoc resmap (first itc) 0))
        (recur (rest itc) (rest iti) (addmap resmap (first itc))))
      resmap)))

(defn filtre-indications-aux [s t i]
  (loop [freqmap_s (frequences s), itt t, iti i]
    (if (seq itt)
      (if (= (first iti) :good)
        (recur (submap freqmap_s (first itt)) (rest itt) (rest iti))
        (recur freqmap_s (rest itt) (rest iti)))
      freqmap_s)))

(defn filtre-indications [s t i]
  (let [freqmap_s (filtre-indications-aux s t i)]
    (loop [itt t, iti i, freqmap {:rouge 0, :vert 0, :bleu 0, :noir 0, :blanc 0, :jaune 0}, res []]
      (if (seq itt)
        (let [t_val (first itt), i_val (first iti)]
          (if (= i_val :color)
            (if (>= (freqmap t_val) (freqmap_s t_val))
              (recur (rest itt) (rest iti) (addmap freqmap t_val) (conj res :bad))
              (recur (rest itt) (rest iti) (addmap freqmap t_val) (conj res :color)))
            (recur (rest itt) (rest iti) freqmap (conj res i_val))))
        res))))

(defn parse-attempt [s]
  (let [s (str/split s #" "), colormap {"rouge" :rouge, "bleu" :bleu, "vert" :vert, "jaune" :jaune, "noir" :noir, "blanc" :blanc}]
    (loop [s s, seq_res []]
      (if (seq s)
        (recur (rest s) (conj seq_res (colormap (first s))))
        seq_res))))

(defn parse-indications [s]
  (let [s (str/split s #" "), indmap {"bon" :good, "position" :color, "mauvais" :bad}]
    (loop [s s, seq_res []]
      (if (seq s)
        (recur (rest s) (conj seq_res (indmap (first s))))
        seq_res))))

(defn translate-indications [s]
  (let [indmap {:bad "mauvais", :good "bon", :color "position"}]
    (loop [s s, str_res ""]
      (if (seq s)
        (recur (rest s) (str str_res " " (indmap (first s))))
        str_res))))

(defn print-code [s]
  (let [colormap {:rouge "rouge", :bleu "bleu", :vert "vert", :jaune "jaune", :noir "noir", :blanc "blanc"}]
    (loop [s s, str_res ""]
      (if (seq s)
        (recur (rest s) (str str_res " " (colormap (first s))))
        (println str_res)))))

(defn make-sol [solution, to_try]
  (loop [sol [], its solution]
    (if (seq its)
      (if (= (first its) :undefined)
        (recur (conj sol (first to_try)) (rest its))
        (recur (conj sol (first its)) (rest its)))
      sol)))

(defn refine-sol [solution indications to_try]
  (loop [its solution, iti indications, to_try to_try, sol_res []]
    (if (seq its)
      (if (= (first iti) :good)
        (recur (rest its) (rest iti) to_try (conj sol_res (first its)))
        (if (= (first iti) :color)
          (recur (rest its) (rest iti) to_try (conj sol_res (first to_try)))
          (let [refined_to_try (disj to_try (first its))]
            (recur (rest its) (rest iti) refined_to_try (conj sol_res (first refined_to_try))))))
      [sol_res, to_try])))

(defn solveur []
  (let [solution [:jaune, :bleu, :rouge, :blanc, :vert], to_try #{:rouge, :bleu, :jaune, :noir, :vert, :blanc}]
    (loop [solution solution, to_try to_try, nb_tentatives 0]
      (if (<= nb_tentatives 12) 
        (do (print-code solution)
            (println "Donnez les indications correspondantes :")
            (let [indications (parse-indications (read-line))]
              ;(println indications)
              (if (= indications [:good, :good, :good, :good, :good])
                (println "Encore gagné !!! C'est vraiment trop facile... Vous voulez rejouer ? (appuyez sur Y pour accepter, n'importe quelle autre touche sinon)")
                (let [refined-sol (refine-sol solution indications to_try)]
                  ;(println refined-sol)
                  (recur (first refined-sol) (second refined-sol) (+ nb_tentatives 1))))))
        (println "Comment !? J'ai perdu !!? Impossible, vous avez triché !! Je demande une revanche ! (appuyez sur Y pour accepter, n'importe quelle autre touche sinon)")))))