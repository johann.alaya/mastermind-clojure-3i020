(ns mastermind.core
  (:gen-class))
(require '[mastermind.aux_functions :as aux])

(defn -main []
  (println "Tapez d pour le mode déchiffreur et c pour le mode codeur : ")
  (if (= (read-line) "d")
    (let [code (aux/code-secret 5)]
      (loop [nb_tentatives 0, code code]
        (if (<= nb_tentatives 12)
          (do (println "Entrez une tentative :")
              (let [tentative (aux/parse-attempt (read-line)), indic (aux/filtre-indications code tentative (aux/indications code tentative))]
                (if (= indic [:good :good :good :good :good])
                  (do (println "Bien joué, vous avez gagné ! Si vous voulez rejouer, appuyez sur Y, sinon appuyez sur n'importe quelle autre touche :")
                      (if (= (read-line) "Y")
                        (recur 0 (aux/code-secret 5))
                        (println "Fin de la partie.")))
                  (do (println (aux/translate-indications indic))
                      (recur (+ nb_tentatives 1) code)))))
          (do (println "Dommage, vous avez perdu... (Nombre maximum de tentatives dépassées) Si vous voulez rejouer, appuyez sur Y, sinon appuyez sur n'importe quelle autre touche :")
              (if (= (read-line) "Y")
                (recur 0 (aux/code-secret 5))
                (println "Fin de la partie."))))))
    (loop [] 
      (aux/solveur)
      (if (= (read-line) "Y")
        (recur)
        (println "Fin de la partie.")))))