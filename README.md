# mastermind

Programme permettant de jouer au mastermind contre l'ordinateur en prenant soit le role du déchiffreur soit le role du codeur.

## Usage

Ouvrir le dossier dans un terminal puis taper lein run pour lancer le programme dans le terminal courant, ou bien taper lein uberjar puis executer le .jar qui se trouvera alors dans target avec la commande : 

    $ java -jar mastermind-0.1.0-standalone.jar

### Problèmes

 - L'ordinateur est trop fort lorsque l'on joue en étant le codeur et gagne toujours en moins de 12 tentatives.

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
